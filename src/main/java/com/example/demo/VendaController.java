package com.example.demo;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/venda")
public class VendaController {
	
	@Autowired
	private VendaRepository vr;
	
	@GetMapping("/get")
	public List<Venda> listarVenda(){
		return vr.findAll();
	}
	
	@GetMapping("/get/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Venda listarVenda(@PathVariable Long id){
		return vr.findById(id).get();
	}
	
	@PostMapping("/post")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Venda> inserirVenda(@RequestBody Venda venda){
		Venda vSalva = vr.save(venda);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(vSalva.getId()).toUri();
		return ResponseEntity.created(uri).body(vSalva);
	}
	
	
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void deletar(@PathVariable Long id) {
		vr.deleteById(id);
	}
	
	@PutMapping("/put/{id}")
	public ResponseEntity<Venda> atualizar(@PathVariable Long id, @Valid @RequestBody Venda venda){
		Optional<Venda> vSalva = vr.findById(id);
		if (!vSalva.isPresent())
			return ResponseEntity.notFound().build();
		BeanUtils.copyProperties(venda, vSalva.get(), "id");
		vr.save(vSalva.get());
		return ResponseEntity.ok(vSalva.get());
	}

}
